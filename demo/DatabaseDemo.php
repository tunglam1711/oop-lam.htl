<?php
require_once '../dao/Database.php';
require_once '../entity/Product.php';

class DatabaseDemo
{
   protected $connectDB;
   const PRODUCT_TABLE = 'product';

   public function __construct()
   {
      $this->connectDB = Database::getInstance();
   }

   /**
    * insert data of table test
    * @return mixed
    */
   public function insertTableTest()
   {
      $product = new Product('3', '2');
      return $this->connectDB->insertTable($product);
   }

   /**
    * display data of table test
    * @return mixed
    */
   public function selectTableTest()
   {
      return $this->connectDB->selectTable(self::PRODUCT_TABLE);
   }

   /**
    * Update table test with sample data
    * 
    */
   public function updateTableTest()
   {
      $product = new Product('id', 'Banana');
      return $this->connectDB->updateTable($product);
   }

   /**
    * Update table test by ID with sample data 
    * @return mixed
    */
   public function updateTableByIdTest()
   {
      return $this->connectDB->updateTableById(self::PRODUCT_TABLE, '2', [2, '3']);
   }

   /**
    * Delete test table with sample data
    * @return mixed
    */
   public function deleteTableTest()
   {
      $product = new Product('2','Mango');
      return $this->connectDB->deleteTable($product);
   }

   /**
    * truncate test table with sample data
    * @return mixed
    */
   public function truncateTableTest()
   {
      return $this->connectDB->truncateTable(self::PRODUCT_TABLE);
   }

   /**
    * init 10 sample data
    * @return mixed
    */
   public function initDatabase()
   {
      for ($i = 0; $i < 10; $i++) {
         $id = rand(1, 100);
         $val = rand(1, 100);
         $product = new Product($id,$val);
         $result = $this->connectDB->insertTable($product);
      }
      return $result;
   }

   /**
    * Display a test table
    * @return mixed
    */
   public function printTableTest()
   {
      $this->updateTableTest();
      echo '<pre>';
      print_r($this->selectTableTest());
      echo '</pre>';
   }
}

$tableTest = new DatabaseDemo;
$tableTest->printTableTest();
