<?php
require_once '../dao/Database.php';
require_once '../entity/Product.php';

class ProductDaoDemo
{
   const PRODUCT_TABLE = 'product';
   protected $connectDB;

   public function __construct()
   {
      $this->connectDB = Database::getInstance();
   }

   /**
    * Insert data sample into product table
    * @param array $row
    * @return mixed
    */
   public function insertTest($row = array())
   {
      return $this->connectDB->insertTable($row);
   }

   /**
    * Display all data of product table
    * @return mixed
    */
   public function findAllTest()
   {
      return $this->connectDB->selectTable(self::PRODUCT_TABLE);
   }

   /**
    * Update data sample into product table
    * @param array $row
    * @return mixed
    */
   public function updateTest($row = array())
   {
      return $this->connectDB->updateTable($row);
   }
}

$product = new Product('1','Banana');
$productDemo = new ProductDaoDemo;
$productDemo->insertTest($product);
echo '<pre>';
print_r($productDemo->findAllTest());
echo '</pre>';
