<?php
require_once '../dao/Database.php';

class CategoryDaoDemo 
{
   const CATEGORY_TABLE = 'category';
   protected $connectDB;

   public function __construct(){
      $this->connectDB = Database::getInstance();
   }

   /**
    * Insert data sample into category table
    * @param array $row
    * @return mixed
    */
   public function insertTest($row=array())
   {
      return $this->connectDB->insertTable($row);
   }

   /**
    * Display all data of category table
    * @return mixed
    */
   public function findAllTest()
   {
      return $this->connectDB->selectTable(self::CATEGORY_TABLE);
   }

   /**
    * Update data sample into category table
    * @param array $row
    * @return mixed
    */
   public function updateTest($row=array())
   {
      return $this->connectDB->updateTable($row);
   }
}
