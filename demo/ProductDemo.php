<?php

require_once '../entity/Product.php';

class ProductDemo extends Product
{
    /**
     * create sample product data
     * @return mixed
     */
   public function createProductTest()
   {
       $product = new Product('','');
       return $product;
   }

   /**
    * display sample product data
    * @param mixed $product
    * @return mixed
    */
   public function printProduct(Product $product)
   {
       echo "Name: ".$product->getName();
       echo "ID: ".$product->getId();
   }
}


$productDataDemo = new ProductDemo(3,'Car');
echo $productDataDemo->printProduct($productDataDemo);
