<?php

require_once '../dao/Database.php';

abstract class BaseDao
{
   protected $row;
   protected $name;
   protected $id;
   protected $connectDB;

   public function __construct($row, $name, $id)
   {
      $this->id = $id;
      $this->name = $name;
      $this->row = $row;
      $this->connectDB = Database::getInstance();
   }

   /**
    * Insert row into the table
    * @param array $row
    * @return mixed
    */
   abstract public function insert($row);
   
   /**
    * Update row into the table
    * @param array $row
    * @return mixed
    */
    abstract public function update($row);

   /**
    * Delete row into the table
    * @param array $row
    * @return mixed
    */
   abstract public function delete($row);

   /**
    * Get all data in the table
    * @return mixed
    */
   protected function findAll($name)
   {
      return $this->connectDB->selectTable($this->name);
   }

   /**
    * Find data by id
    * @param string $id
    * @return mixed
    */
   abstract public function findById($id);

   /**
    * Find data by name
    * @param mixed $name
    *  @return mixed
    */
   abstract public function findByName($name);
}
