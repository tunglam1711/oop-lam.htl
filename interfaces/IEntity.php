<?php
interface IEntity
{
   /**
    * set ID 
    * @param int $id
    * @return mixed
    */
   public function setId($id);

   /**
    * get ID
    * @return mixed
    */
   public function getId();

   /**
    * set Name
    * @param string $name
    * @return mixed
    */
   public function setName($name);

   /**
    * get Name
    * @return mixed
    */
   public function getname();
}
