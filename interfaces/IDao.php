<?php

interface IDao
{
   /**
    * insert row into table
    * @param mixed $row
    * @return mixed
    */
   public function insert($row);

   /**
    * update row into table
    * @param mixed $row
    * @return mixed
    */
   public function update($row);

   /**
    * delete row into the table
    * @param mixed $row
    * @return mixed
    */
   public function delete($row);

   /**
    * Get all data in the table
    * @return mixed
    */
   public function findAll();

   /**
    * find data by id
    * @param mixed $id
    * @return mixed
    */
   public function findById($id);

   /**
    * find data by name
    * @param string $name
    * @return mixed */
   public function findByName($name);
}
