<?php

require_once '../dao/Database.php';
require_once '../BaseDao/BaseDao.php';

class ProductDAO extends BaseDao
{
   const PRODUCT_TABLE = 'product';

   /**
    * Insert data into product table
    * @param mixed $row
    * @return mixed
    */
   public function insert($row)
   {
      return $this->connectDB->insertTable($row);
   }

   /**
    * Update data into product table
    * @param mixed $row
    * @return mixed
    */
   public function update($row)
   {
      return $this->connectDB->updateTable($row);
   }

   /**
    * Delete data into product table
    * @param mixed $row
    * @return mixed
    */
   public function delete($row)
   {
      return $this->connectDB->deleteTable($row);
   }

   /**
    * Get all data of Product Table
    * @return mixed
    */
   public function findAllCategory()
   {
      return $this->findAll(self::PRODUCT_TABLE);
   }

   /**
    * Find data by id
    * @param mixed $id
    * @return mixed
    */
   public function findById($id)
   {
      $result = array();
      foreach ($this->connectDB->productTable as $key => $value) {
         if ($value == $id) {
            $result = $value;
         }
      }
      return $result;
   }

   /**
    * Find data by name
    * @param mixed $row
    * @return mixed
    */
   public function findByName($name)
   {
      $result = array();
      foreach ($this->connectDB->productTable as $key => $value) {
         if ($value == $name) {
            $result = $key;
         }
      }
      return $result;
   }
}
