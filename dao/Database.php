<?php

require_once '../entity/Accessory.php';
require_once '../entity/Category.php';
require_once '../entity/Product.php';

class Database
{
   protected array $productTable = array(['2'=>'Mango','Melon','Coconut','Apple']);
   protected array $categoryTable = array();
   protected array $accessoryTable = array();

   const PRODUCT_TABLE = 'product';
   const CATEGORY_TABLE = 'category';
   const ACCESSORY_TABLE = 'accessory';

   private static $instance = null;

   private function __construct()
   {
   }

   public static function getInstance()
   {
      if (static::$instance == null) {
         static::$instance = new Database;
      }
      return static::$instance;
   }

   /**
    * get class of object
    * @param mixed $row
    * @return mixed
    */
   public function getClassObject($row)
   {
      return get_class($row);
   }

   /**
    * Insert data into Table
    * @param mixed $row
    * @return mixed
    */
   public function insertTable($row)
   {
      $className = $this->getClassObject($row);
      if ($className == self::PRODUCT_TABLE) {
         return array_push($this->productTable, $row);
      } elseif ($className == self::CATEGORY_TABLE) {
         return array_push($this->categoryTable, $row);
      } elseif ($className == self::ACCESSORY_TABLE) {
         return array_push($this->accessoryTable, $row);
      }
   }

   /**
    * Select and display Table
    * @param string $name
    * @return mixed
    */
   public function selectTable($name)
   {
      if ($name == self::PRODUCT_TABLE) {
         return $this->productTable;
      } elseif ($name == self::CATEGORY_TABLE) {
         return $this->categoryTable;
      } elseif ($name == self::ACCESSORY_TABLE) {
         return $this->accessoryTable;
      }
   }

   /**
    * Update data into Table
    * @param mixed $row
    * @return mixed
    */
   public function updateTable($row)
   {
      $className = $this->getClassObject($row);
      if ($className == self::PRODUCT_TABLE) {
         return $this->updateRow($this->productTable, $row);
      } elseif ($className == self::CATEGORY_TABLE) {
         return $this->updateRow($this->categoryTable, $row);
      } elseif ($className == self::ACCESSORY_TABLE) {
         return $this->updateRow($this->accessoryTable, $row);
      }
   }

   /**
    * Update data with key id into Table
    * @param mixed $table
    * @param mixed $row
    * @return mixed
    */
   private function updateRow(&$table, $row)
   {
      foreach ($table as $key => $item) {
         if ($key == 'id') {
            $table[$key] = $row['id'];
            return;
         }
      }
   }

   /**
    * Delete a table
    * @param mixed $row
    * @return mixed
    */
   public function deleteTable($row)
   {
      $id = $row['id'];
      $className = $this->getClassObject($row);

      if ($className == self::PRODUCT_TABLE) {
         return $this->deleteRow($this->productTable, $id);
      } elseif ($className == self::CATEGORY_TABLE) {
         return $this->deleteRow($this->categoryTable, $id);
      } elseif ($className == self::ACCESSORY_TABLE) {
         return $this->deleteRow($this->accessoryTable, $id);
      }
   }

   /**
    * Handle Delete table with key id
    * @param mixed $table
    * @param mixed $row
    * @return
    */
   private function deleteRow(&$table, $id)
   {
      foreach ($table as $key => $value) {
         if ($key == 'id') {
            unset($table[$key]);
            return;
         }
      }
   }

   /**
    * Truncate a table
    * @param mixed $name
    * @return mixed
    */
   public function truncateTable($name)
   {
      if ($name == self::PRODUCT_TABLE) {
         $this->productTable = array();
      } elseif ($name == self::CATEGORY_TABLE) {
         $this->categoryTable = array();
      } elseif ($name == self::ACCESSORY_TABLE) {
         $this->accessoryTable = array();
      }
   }

   /**
    * update table with id entered
    * @param mixed $name
    * @param mixed $id
    * @param mixed $row
    * @return mixed
    */
   public function updateTableById($name, $id, $row)
   {
      if ($name == self::PRODUCT_TABLE) {
         return $this->updateById($this->productTable, $id, $row);
      } elseif ($name == self::CATEGORY_TABLE) {
         return $this->updateById($this->categoryTable, $id, $row);
      } elseif ($name == self::ACCESSORY_TABLE) {
         return $this->updateById($this->accessoryTable, $id, $row);
      }
   }

   /**
    * handle table update with id entered
    * @param mixed $name
    * @param mixed $id
    * @param mixed $row
    * @return mixed
    */
   private function updateById($name, $id, $row)
   {
      foreach ($name as $key => $item) {
         if ($key == $id) {
            $name[$id] = $row[$id];
            break;
         }
      }
   }
}
