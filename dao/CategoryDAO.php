<?php

require_once '../dao/Database.php';
require_once '../BaseDao/BaseDao.php';

class CategoryDAO extends BaseDao
{  
   const CATEGORY_TABLE = 'category';
   
   /**
    * Insert data into category table
    * @param mixed $row
    * @return mixed
    */
   public function insert($row)
   {
      return $this->connectDB->insertTable($row);
   }

   /**
    * Update data into category table
    * @param mixed $row
    * @return mixed
    */
   public function update($row)
   {
      return $this->connectDB->insertTable($row);
   }

   /**
    * Insert data into category table
    * @param mixed $row
    * @return mixed
    */
   public function delete($row)
   {
      return $this->connectDB->deleteTable($row);
   }
   /**
    * get all data of Category Table
    * @return mixed
    */
   public function findAllCategory()
   {
      return $this->findAll(self::CATEGORY_TABLE);
   }

   /**
    * Find data by id
    * @param mixed $id
    * @return mixed
    */
   public function findById($id)
   {
      $result = array();
      foreach ($this->connectDB->categoryTable as $key => $value) {
         if ($value == $id) {
            $result = $value;
         }
      }
      return $result;
   }

    /**
    * Find data by name
    * @param mixed $row
    * @return mixed
    */
   public function findByName($name)
   {
      $result = array();
      foreach ($this->connectDB->categoryTable as $key => $value) {
         if ($value == $name) {
            $result = $key;
         }
      }
      return $result;
   }
}
