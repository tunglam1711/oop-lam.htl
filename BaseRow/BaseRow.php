<?php 
abstract class BaseRow
{
   protected $id;
   protected $name;

   /**
    * set ID
    * @param mixed $id
    * @return mixed
    */
   public function setID($id){
      $this->id = $id;
   }

   abstract public function getID();
   

   /**
    * set Name
    * @param string $name
    * @return mixed
    */
   public function setName($name){
      $this->name = $name;
   }

   abstract public function getName();

}
