<?php

require_once '../BaseRow/BaseRow.php';

class Category extends BaseRow
{
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * get ID
     * @return mixed
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * get Name
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
}
